package main.java.ExportBehavior;

import main.java.Order;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class ExportToPlainText implements ExportBehavior {
    Order order;

    public ExportToPlainText(Order order) {
        this.order = order;
    }

    @Override
    public void export(){
        try {
            String filename = "output/output.txt";
            PrintWriter outputStream = new PrintWriter(filename);
            outputStream.println("Order Number: " + order.getOrderNr());
            outputStream.println("Student:  " + order.isStudentOrder());
            outputStream.close();
        } catch(FileNotFoundException e){
            e.printStackTrace();
        }
    }
}
