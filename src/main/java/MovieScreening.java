package main.java;

import java.time.LocalDateTime;

public class MovieScreening {
     LocalDateTime dateAndTime;
     double pricePerSeat;
     Movie movie;
    public MovieScreening(Movie movie, LocalDateTime dateAndTime, double pricePerSeat) {
        this.dateAndTime = dateAndTime;
        this.pricePerSeat = pricePerSeat;
        this.movie = movie;
    }

    public LocalDateTime getDateAndTime() {
        return dateAndTime;
    }

    public double getPricePerSeat() {
        return pricePerSeat;
    }
}
