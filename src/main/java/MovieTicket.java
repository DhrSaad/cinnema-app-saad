package main.java;
import java.time.DayOfWeek;

public class MovieTicket  {
     int rowNr;
     int seatNr;
     boolean isPremium;
    MovieScreening screening;
    public MovieTicket(MovieScreening movieScreening, int rowNr, int seatNr, boolean isPremium) {
        this.rowNr = rowNr;
        this.seatNr = seatNr;
        this.isPremium = isPremium;
        this.screening = movieScreening;
    }

    @Override
    public String toString() {
        return "MovieTicket{" +
                "rowNr=" + rowNr +
                ", seatNr=" + seatNr +
                ", isPremium=" + isPremium +
                '}';
    }

    public boolean isPremium() {
        return isPremium;
    }
    public double getPrice(){
        return screening.getPricePerSeat();

    }
    public DayOfWeek dayOfWeek() {
        var day_of_week =  screening.dateAndTime.getDayOfWeek();
        System.out.println("Day of week: " + day_of_week +" value: "+day_of_week.getValue());
        return day_of_week;
    }
}
