package main.java;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;


public class Order {
     int orderNr;
     boolean isStudentOrder;
    ArrayList<MovieTicket> tickets = new ArrayList<>();

    public Order(int orderNr, boolean isStudentOrder) {
        this.orderNr = orderNr;
        this.isStudentOrder = isStudentOrder;
    }
    public void addSeatReservation(MovieTicket ticket) {
        tickets.add(ticket);
    }

    public int getOrderNr() {
        return orderNr;
    }

    public boolean isStudentOrder() {
        return isStudentOrder;
    }

    public double calculatePrice() {

        var price = 0.0;
        for (int i = 0; i < tickets.size(); i++) {
            var ticket = tickets.get(i);
            var ticketprice = ticket.getPrice();
            // eerst premium erbij tellen, dan pas korting
            if (tickets.get(i).isPremium()) {
                if (isStudentOrder) {
                    ticketprice = ticketprice + 2.0;
                } else {
                    ticketprice = ticketprice + 3.0;
                }
            }
            var day_of_week = ticket.dayOfWeek().getValue();
            if (!(i == 1 && (day_of_week >= 1 && day_of_week <= 4) || (i == 1 && isStudentOrder))) {
                price += ticketprice;
            } else {
                System.out.println("Gefeliciteed, gratis tweede ticket!!");
            }
        }
        if (tickets.size() >= 6 || isStudentOrder) {
            price *= 0.9;
        }

        return price;
    }
    public enum TicketExportFormat {
        PLAINTEXT,
        JSON
    }

    public void export(TicketExportFormat exportFormat) {
        if(exportFormat.equals(TicketExportFormat.PLAINTEXT)){
            try {
                String filename = "output/output.txt";
                PrintWriter outputStream = new PrintWriter(filename);
                outputStream.println("Order Number: " + orderNr);
                outputStream.println("Student:  " + isStudentOrder);
                outputStream.close();
            } catch(FileNotFoundException e){
                e.printStackTrace();
            }
        } else if(exportFormat.equals(TicketExportFormat.JSON)){
            // Cannot get JSONObject working.
            System.out.println("JSON!");
        }

    }


}
