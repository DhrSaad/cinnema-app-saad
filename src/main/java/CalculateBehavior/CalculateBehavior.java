package main.java.CalculateBehavior;

import main.java.MovieTicket;

import java.util.ArrayList;

public interface CalculateBehavior {
    double calculatePrice(ArrayList<MovieTicket> tickets);
}
