package test.java;

import main.java.Movie;
import org.junit.Assert;
import org.junit.Test;

public class MovieTest {
    @Test
    public void movie_to_string_should_return_name() {
        var MovieTT= new Movie("Forrest Gump");
        Assert.assertEquals("Forrest Gump",MovieTT.toString());
    }
}
